﻿namespace Exercise_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.marsWeight = new System.Windows.Forms.TextBox();
            this.submitEarth = new System.Windows.Forms.Button();
            this.earthText = new System.Windows.Forms.Label();
            this.marsTag = new System.Windows.Forms.Label();
            this.earthWeight = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.submitMars = new System.Windows.Forms.Button();
            this.calculationReminder2 = new System.Windows.Forms.Label();
            this.calculationReminder1 = new System.Windows.Forms.Label();
            this.calculationReminder3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // marsWeight
            // 
            this.marsWeight.Location = new System.Drawing.Point(263, 129);
            this.marsWeight.Name = "marsWeight";
            this.marsWeight.Size = new System.Drawing.Size(139, 20);
            this.marsWeight.TabIndex = 1;
            // 
            // submitEarth
            // 
            this.submitEarth.Location = new System.Drawing.Point(84, 155);
            this.submitEarth.Name = "submitEarth";
            this.submitEarth.Size = new System.Drawing.Size(139, 23);
            this.submitEarth.TabIndex = 2;
            this.submitEarth.Text = "Calculate Weight on Mars";
            this.submitEarth.UseVisualStyleBackColor = true;
            this.submitEarth.Click += new System.EventHandler(this.submit_Click_1);
            // 
            // earthText
            // 
            this.earthText.AutoSize = true;
            this.earthText.BackColor = System.Drawing.Color.Green;
            this.earthText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.earthText.Location = new System.Drawing.Point(112, 111);
            this.earthText.Name = "earthText";
            this.earthText.Size = new System.Drawing.Size(80, 15);
            this.earthText.TabIndex = 3;
            this.earthText.Text = "Earth Weight:";
            this.earthText.Click += new System.EventHandler(this.label1_Click);
            // 
            // marsTag
            // 
            this.marsTag.AutoSize = true;
            this.marsTag.BackColor = System.Drawing.Color.Firebrick;
            this.marsTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.marsTag.Location = new System.Drawing.Point(293, 111);
            this.marsTag.Name = "marsTag";
            this.marsTag.Size = new System.Drawing.Size(79, 15);
            this.marsTag.TabIndex = 4;
            this.marsTag.Text = "Mars Weight:";
            // 
            // earthWeight
            // 
            this.earthWeight.Location = new System.Drawing.Point(84, 129);
            this.earthWeight.Name = "earthWeight";
            this.earthWeight.Size = new System.Drawing.Size(139, 20);
            this.earthWeight.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(-8, -34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(495, 342);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // submitMars
            // 
            this.submitMars.Location = new System.Drawing.Point(263, 155);
            this.submitMars.Name = "submitMars";
            this.submitMars.Size = new System.Drawing.Size(139, 23);
            this.submitMars.TabIndex = 6;
            this.submitMars.Text = "Calculate Weight on Earth";
            this.submitMars.UseVisualStyleBackColor = true;
            this.submitMars.Click += new System.EventHandler(this.button1_Click);
            // 
            // calculationReminder2
            // 
            this.calculationReminder2.AutoSize = true;
            this.calculationReminder2.BackColor = System.Drawing.Color.Gainsboro;
            this.calculationReminder2.Location = new System.Drawing.Point(107, 275);
            this.calculationReminder2.Name = "calculationReminder2";
            this.calculationReminder2.Size = new System.Drawing.Size(277, 13);
            this.calculationReminder2.TabIndex = 7;
            this.calculationReminder2.Text = "When converting weights, the results will be rounded to 3\r\n";
            // 
            // calculationReminder1
            // 
            this.calculationReminder1.AutoSize = true;
            this.calculationReminder1.BackColor = System.Drawing.Color.Gainsboro;
            this.calculationReminder1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calculationReminder1.Location = new System.Drawing.Point(206, 258);
            this.calculationReminder1.Name = "calculationReminder1";
            this.calculationReminder1.Size = new System.Drawing.Size(81, 17);
            this.calculationReminder1.TabIndex = 8;
            this.calculationReminder1.Text = "Remember:";
            // 
            // calculationReminder3
            // 
            this.calculationReminder3.AutoSize = true;
            this.calculationReminder3.BackColor = System.Drawing.Color.Gainsboro;
            this.calculationReminder3.Location = new System.Drawing.Point(148, 288);
            this.calculationReminder3.Name = "calculationReminder3";
            this.calculationReminder3.Size = new System.Drawing.Size(199, 13);
            this.calculationReminder3.TabIndex = 9;
            this.calculationReminder3.Text = "decimal places and thus be more precise";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(478, 304);
            this.Controls.Add(this.calculationReminder3);
            this.Controls.Add(this.calculationReminder1);
            this.Controls.Add(this.calculationReminder2);
            this.Controls.Add(this.submitMars);
            this.Controls.Add(this.marsTag);
            this.Controls.Add(this.earthText);
            this.Controls.Add(this.submitEarth);
            this.Controls.Add(this.marsWeight);
            this.Controls.Add(this.earthWeight);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Weight on Mars";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox marsWeight;
        private System.Windows.Forms.Button submitEarth;
        private System.Windows.Forms.Label earthText;
        private System.Windows.Forms.Label marsTag;
        private System.Windows.Forms.TextBox earthWeight;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button submitMars;
        private System.Windows.Forms.Label calculationReminder2;
        private System.Windows.Forms.Label calculationReminder1;
        private System.Windows.Forms.Label calculationReminder3;
    }
}

