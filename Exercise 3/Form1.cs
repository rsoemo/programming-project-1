﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            submitEarth.Click += new EventHandler(submitConversion);  //Checking for submit click from earth
            submitMars.Click += new EventHandler(submitConversion);  //Checking for submit click from mars
        }

        private void submitConversion(Object sender, EventArgs e)  //Code run when submit is clicked
        {
            double formula = 1;  //Storage of formula
            string input = "";  //Input string
            TextBox outputTo = null;  //Which textbox to output to *
            double translatedInput = 0; //Setting up variable to store converted number

            if (sender == submitEarth)  //If submission is converting to mars weight
            {
                input = earthWeight.Text;  //Grabbing input from text box
                formula =  1 / 9.81 * 3.711;  //Calculation of formula in the weight of m/s^2, gotten from http://learningaboutelectronics.com/Articles/Weight-on-mars-conversion-calculator.php#answer
                outputTo = marsWeight;  //Selecting to output to mars' weight box
            }
            else if (sender == submitMars)  ////If submission is converting to earth weight
            {
                input = marsWeight.Text;  //Grabbing input from text box
                formula = 1 / 3.711 * 9.81;  //Inverse formula of earth to mars weight calculation
                outputTo = earthWeight;  //Selecting to output to earth's weight box
            }

            if (input != "")  //Checking if there is no input
            {
                try  //Trying to parse string into int
                {
                    translatedInput = double.Parse(input);  //Translating string into an double
                    double output = Math.Round(translatedInput * formula, 3, MidpointRounding.AwayFromZero);  //Runs formula and rounds to the 3rd decimal place
                    string outputString = output.ToString();  //Translating double back into string
                    outputTo.Text = outputString;  //Outputting string to text box
                }
                catch (Exception)  //Catching error
                {
                    string errorTitle = "Non-Numeric Symbols Detected";  //Title for alert window
                    string errorMessage = "Only use numeric values and not alphabetic symbols in the weight calculation input.";  //Message for alert window
                    MessageBoxButtons button = MessageBoxButtons.OK; //Button for alert window
                    DialogResult result;  //Setting up alert window
                    result = MessageBox.Show(errorMessage, errorTitle, button);  //Running alert window
                }
            }
            else
            {
                string errorTitle = "No Input";  //Title for alert window
                string errorMessage = "Enter in a numeric value into the weight calculation input";  //Message for alert window
                MessageBoxButtons button = MessageBoxButtons.OK; //Button for alert window
                DialogResult result;  //Setting up alert window
                result = MessageBox.Show(errorMessage, errorTitle, button);  //Running alert window
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void submit_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
